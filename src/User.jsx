import React from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';


const User = () => {

    const {name} = useParams();

    const location = useLocation();

    const history = useHistory();

    return (
        <>
            <h1>User is {name} and the location is {location.pathname} </h1>
            {location.pathname === "/user/vrund" ? 
            <button onClick={()=> history.goBack()}>go back</button> : null } 
        </>
    )

}

export default User;