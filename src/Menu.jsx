import React from 'react';
import {NavLink} from 'react-router-dom';
import './index.css';

const Menu = () => {

    return(
        <>
            <NavLink exact activeClassName="active" to="/">Home</NavLink>
            <NavLink exact activeClassName="active" to="/about">About</NavLink>
            <NavLink exact activeClassName="active" to="/user/vrund">User</NavLink>
        </>
    )

}


export default Menu;