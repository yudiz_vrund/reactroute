import React from 'react';
import { Route, Switch } from 'react-router';
import Home from './Home';
import About from './About';
import User from './User';
import Menu from './Menu';

function App() {
  return (
    <>
    <Menu/>
    <Switch>
      <Route path="/" component={Home} exact/>
      <Route path="/about" component={About}/>
      <Route path="/user/:name" component={User}/>
    </Switch>
    </>
  );
}

export default App;
